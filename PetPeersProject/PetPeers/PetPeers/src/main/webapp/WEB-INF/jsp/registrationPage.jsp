<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>registrationPage</title>
<style>
.boxed {
	background: black;
	border: 1px;
	color: white;
	padding: 5px;
	padding-left: 8px;
	width: 100%;
}

.center {
	margin: 50px 300px;
}

i {
	position: absolute;
	right: 325px;
	color: white;
}

form {
	padding: 0px 50px;
}

input[type=text], [type=password] {
	border-radius: 4px;
	border: 2px solid #DCDCDC;
	width: 120%;
}

table {
	border-spacing: 0 10px;
}

a {
	color: white;
	text-decoration: none;
}

.button {
	width: fit-content;
	text-decoration: none;
	color: #fff;
	border: #6c757d;
	background: blue;
	border-radius: .25rem;
	font-size: 1rem;
	padding: .375rem .75rem;
	margin-top: 10px;
}
</style>
</head>
<body>
	<div class="center">

		<div class="boxed">
			Pet Shop<i class="fa fa-sign-in"><a href="login"> Login</a></i>

		</div>

		<form:form method="post" action="saveUser" modelAttribute="userModel">


			<table align="left">
				<tr>
					<td>Register</td>
				</tr>
				<tr>
					<td>Name:</td>
				</tr>
				<tr>
					<td><form:input path="userName" type="text" size="100%" /></td>
				</tr>
				<tr>
					<td><form:errors path="userName" cssStyle="color: #ff0000"></form:errors></td>
				</tr>
				<td>Password:</td>
				</tr>
				<tr>
					<td><form:input path="userPassword" type="password"
							size="100%" /></td>
				</tr>

				<tr>
					<td><form:errors path="userPassword" cssStyle="color: #ff0000"></form:errors></td>
				</tr>

				<tr>
					<td>Confirm Password:</td>
				</tr>
				<tr>
					<td><form:input path="confirmPassword" type="password"
							size="100%" /></td>
				</tr>
				</tr>
				<tr>
					<td><form:errors path="confirmPassword"
							cssStyle="color: #ff0000"></form:errors></td>
				</tr>


				<tr>
					<td><input type="submit" value="Register" class="button"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>


