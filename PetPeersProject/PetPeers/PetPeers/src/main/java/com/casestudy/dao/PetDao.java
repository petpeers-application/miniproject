package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;

public interface PetDao {
	
	public abstract  List<Pet>getAllPets();
	
	public abstract Pet savePet(Pet pet);
	public abstract Pet buyPet(int petId, int userId) ;


	public abstract List<Pet> getMyPets(int petId);

}
