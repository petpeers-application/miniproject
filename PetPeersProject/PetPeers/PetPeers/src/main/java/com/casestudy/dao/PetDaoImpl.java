package com.casestudy.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.model.Pet;

@Repository
public class PetDaoImpl implements PetDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public Pet savePet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return pet;
		
	}
	
	
	@Override 

	public List<Pet> getAllPets() { 

	 Session session = sessionFactory.getCurrentSession(); 

	 Query<Pet> query= session.createQuery("from pet",Pet.class); 

	     List<Pet> listOfPets =(query).getResultList(); 

	        return listOfPets; 

	        } 

	
	
	/*
	 * @Override public List<Pet> getAllPets() {
	 * 
	 * Session session = sessionFactory.getCurrentSession(); List<Pet> pets =
	 * session.createQuery("from Pet").list(); return pets; }
	 */

	@Override 

	public List<Pet> getMyPets(int petId) { 

	// TODO Auto-generated method stub 

	return null; 

	} 
	@Override 

	public Pet buyPet(int petId, int userId) { 

	// TODO Auto-generated method stub 

	return null; 

	} 

	 

}
