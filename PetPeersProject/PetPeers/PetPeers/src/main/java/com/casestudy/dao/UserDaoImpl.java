package com.casestudy.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public User saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		return user;
	}

	@Override
	@Transactional
	public User authenticateUser(String userName, String password) {

		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(" select userPassword from User where userName=:userName");
		query.setParameter("userName", userName);
		List<User> userPassword = query.list();

		if (userPassword.contains(password)) {

			return new User();

		}

		return null;
	}
}
