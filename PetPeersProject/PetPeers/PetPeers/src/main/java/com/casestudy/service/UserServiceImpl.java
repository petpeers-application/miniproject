package com.casestudy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.casestudy.dao.UserDao;
import com.casestudy.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User saveUser(User user) {
		userDao.saveUser(user);
		return user;
	}

	@Override
	public User authenticateUser(String userName, String password) {
		User user = userDao.authenticateUser(userName, password);
		return user;
	}

}
