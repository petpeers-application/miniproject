package com.casestudy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dao.PetDao;
import com.casestudy.model.Pet;

@Service
@Transactional
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDao petDao;

	@Override

	public List<Pet> getAllPets() {

		List<Pet> list = petDao.getAllPets();

		return list;

	}

	@Override
	public Pet savePet(Pet pet) {

		return petDao.savePet(pet);
	}

	@Override

	public List<Pet> getMyPets(int petId) {

		List<Pet> list = petDao.getMyPets(petId);

		return list;

	}

	@Override

	public Pet buyPet(int petId, int userId) {

		Pet pet = petDao.buyPet(petId, userId);

		return pet;

	}
}
